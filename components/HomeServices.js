import home from "../public/static/images/home.jpg";
import Image from "next/image";
import UtilTitle from "./UtilTitle";
import useTranslation from "next-translate/useTranslation";

export default function HomeServices() {
  const { t } = useTranslation("common");
  return (
    <div>
      <UtilTitle text={t("services.title")} />
      <div className="grid grid-rows-12 grid-cols-4 grid-flow-row gap-y-4 gap-x-10">
        <div className="col-span-3 row-span-4 relative">
          <div className="relative h-[300px]">
            <Image src={home} alt={home} objectFit="cover" layout="fill" />
          </div>
        </div>
        <div className="col-span-1 row-span-4 relative">
          <div className="relative h-[300px]">
            <Image src={home} alt={home} objectFit="cover" layout="fill" />
          </div>
        </div>
        <div className="col-span-1 row-span-8 relative">
          <div className="relative h-[600px]">
            <Image src={home} alt={home} objectFit="cover" layout="fill" />
          </div>
        </div>
        <div className="col-span-2 row-span-8 relative">
          <div className="relative h-[600px]">
            <Image src={home} alt={home} objectFit="cover" layout="fill" />
          </div>
        </div>
        <div className="col-span-1 row-span-8 relative">
          <div className="relative h-[600px]">
            <Image src={home} alt={home} objectFit="cover" layout="fill" />
          </div>
        </div>
      </div>
    </div>
  );
}
