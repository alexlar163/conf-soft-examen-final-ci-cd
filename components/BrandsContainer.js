import BrandsInfo from "./BrandsInfo";
import useTranslation from "next-translate/useTranslation";
import AboutTaller from "./AboutTaller";
import CatedralLogo from "../public/static/images/CatedralLogo.png";
import LogoTrompo from "../public/static/images/LogoTrompo.png";
import UnoCero from "../public/static/images/UnoCero.png";
import HomeInformation from "./HomeInformation";
import ToctePlaceholder from "../public/static/images/ToctePlaceholder.png";

export default function BrandsContainer() {
  const { t } = useTranslation("common");
  return (
    <>
      <HomeInformation
        img={ToctePlaceholder}
        title={t("brands.title")}
        description={t("brands.description")}
      />
      <BrandsInfo text={t("brands.info.trompo")} img={LogoTrompo} />
      <BrandsInfo text={t("brands.info.catedral")} img={CatedralLogo} />
      <BrandsInfo text={t("brands.info.unocero")} img={UnoCero} />
      <AboutTaller showTitle={false} />
    </>
  );
}
