export default function UtilTitle({ text }) {
  return (
    <p className="text-center text-2xl font-bold sm:text-5xl uppercase my-10 text-slate-500">
      {text}
    </p>
  );
}
