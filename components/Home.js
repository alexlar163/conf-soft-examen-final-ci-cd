import HomeBrands from "../components/HomeBrands";
import HomeContact from "../components/HomeContact";
import HomeServices from "../components/HomeServices";
import useTranslation from "next-translate/useTranslation";
import HomeInformation from "./HomeInformation";
import ToctePlaceholder from "../public/static/images/ToctePlaceholder.png";

export default function Home() {
  const { t } = useTranslation("common");
  return (
    <>
      <HomeInformation
        img={ToctePlaceholder}
        title={t("header.title")}
        description={t("header.description")}
      />
      <HomeBrands />
      <HomeServices />
      <HomeContact />
    </>
  );
}
