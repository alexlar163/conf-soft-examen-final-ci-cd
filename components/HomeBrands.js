import Image from "next/image";
import CatedralLogo from "../public/static/images/CatedralLogo.png";
import LogoTrompo from "../public/static/images/LogoTrompo.png";
import UnoCero from "../public/static/images/UnoCero.png";
import catedral from "../public/static/images/catedral.jpg";
import trompo from "../public/static/images/trompo.jpg";
import unocero from "../public/static/images/unocero.jpg";
import useTranslation from "next-translate/useTranslation";
import UtilTitle from "./UtilTitle";

export default function HomeBrands() {
  const { t } = useTranslation("common");
  const brands = [
    {
      brand: catedral,
      img: LogoTrompo,
    },
    {
      brand: trompo,
      img: CatedralLogo,
    },
    {
      brand: unocero,
      img: UnoCero,
    },
  ];
  return (
    <div className="m-5 sm:m-0 sm:mt-5">
      <UtilTitle text={t("brands.title")} />
      <div className="flex flex-col gap-12 mt-5">
        {brands.map((brand, item) => {
          return (
            <div className="block lg:flex lg:flex-row lg:ml-32" key={item}>
              <div className="h-[430px] object-cover relative lg:flex-1">
                <Image
                  src={brand.brand}
                  alt={brand.name}
                  objectFit="cover"
                  layout="fill"
                />
              </div>
              <div className="object-cover relative mx-20 sm:mx-0 mt-12 lg:flex-1 lg:ml-44 lg:my-auto">
                <Image
                  src={brand.img}
                  alt={brand.name}
                  objectFit="cover"
                  layout="responsive"
                />
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
