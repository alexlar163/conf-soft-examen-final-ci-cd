import Image from "next/image";
import UtilButton from "./UtilButton";
import useTranslation from "next-translate/useTranslation";

export default function HomeInformation({img, title, description}) {
  const { t } = useTranslation("common");
  return (
    <div className="container px-10 lg:px-0 sm:m-auto">
      <div className="lg:px-32 flex flex-col-reverse xl:flex-row-reverse">
        <div className="lg:flex-2 xl:px-0 my-auto lg:w-[720px] relative flex items-center justify-center">
          <Image
            src={img}
            alt={img}
            objectFit="cover"
          />
        </div>
        <div className="sm:flex-1 md:mx-32 flex flex-col justify-center items-center gap-10">
          <p className="font-bold text-center uppercase sm:text-right text-4xl sm:text-7xl text-slate-500">
            {title}
          </p>
          <p className="text-center sm:text-right text-xl font-light text-slate-500">
            {description}
          </p>
          <UtilButton
            text={t("header.contact")}
            action={() => {
              window.scrollTo({
                top: document.body.scrollHeight,
                behavior: "smooth",
              });
            }}
          />
        </div>
      </div>
    </div>
  );
}
