import useTranslation from "next-translate/useTranslation";

export default function HomeContactInfo() {
  const { t } = useTranslation("common");
  return (
    <div className="my-auto mx-14 xl:mx-32">
      <p className="text-right text-2xl font-bold sm:text-6xl text-slate-500">
        {t("contact.contact_info.title")}
      </p>
      <p className="text-right text-2xl mt-10 font-semibold text-slate-500">{t("contact.contact_info.description")}</p>
    </div>
  );
}
