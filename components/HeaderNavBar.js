import Link from "next/link";
import Image from "next/image";
import LogoTocteNaranja from "../public/static/images/LogoTocteNaranja.png";
import useTranslation from "next-translate/useTranslation";
import HeaderDropMenu from "./HeaderDropMenu";

export default function HeaderNavBar() {
  const { t } = useTranslation("common");
  return (
    <>
      <div className="container m-auto text-2xl hidden py-5 sm:flex sm:flex-col sm:items-center lg:flex-row lg:justify-between">
        <div className="w-44">
          <Image
            src={LogoTocteNaranja}
            alt={LogoTocteNaranja}
            objectFit="cover"
          />
        </div>
        <nav className="flex justify-center items-center space-x-4">
          {[
            [t("router.home"), "/"],
            [t("router.brands"), "/brands"],
            [t("router.services"), "/services"],
            [t("router.about"), "/about"],
            [t("router.contact"), "/contact"],
          ].map(([title, url]) => (
            <Link href={url} key={`${title}+${url}`}>
              <a className="rounded-lg px-3 py-3 text-slate-500 font-bold hover:bg-slate-100 hover:text-slate-900 uppercase">
                {title}
              </a>
            </Link>
          ))}
        </nav>
      </div>
      <div className="flex flex-col items-center sm:hidden bg-gray-200 relative">
        <HeaderDropMenu />
        <div className="py-10">
          <Image
            src={LogoTocteNaranja}
            alt={LogoTocteNaranja}
            objectFit="cover"
          />
        </div>
      </div>
      
    </>
  );
}
