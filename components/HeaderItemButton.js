import avatar from "../public/static/images/avatar.jpg";
import Image from "next/image";

const HeaderItemButton = () => (
  <div className="relative w-10 h-10">
    <Image
      className="inline-block rounded-full ring-2 ring-white"
      src={avatar}
      alt={avatar}
      objectFit="cover"
      layout="fill"
    />
  </div>
);

export default HeaderItemButton;
