import useTranslation from "next-translate/useTranslation";
import UtilButton from "./UtilButton";

export default function HomeContactForm() {
  const { t } = useTranslation("common");
  return (
    <form className="my-auto mx-14 xl:mx-32">
      <div className="mb-6">
        <label
          htmlFor="name"
          className="block mb-2 text-sm font-medium text-slate-500 dark:text-gray-300"
        >
          {t("contact.your_name")}
        </label>
        <input
          type="text"
          id="name"
          className="bg-gray-50 border border-gray-300 text-slate-500 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          placeholder={t("contact.contact_example.name")}
          required
        />
      </div>
      <div className="mb-6">
        <label
          htmlFor="telephone"
          className="block mb-2 text-sm font-medium text-slate-500 dark:text-gray-300"
        >
          {t("contact.your_telephone")}
        </label>
        <input
          type="text"
          id="telephone"
          className="bg-gray-50 border border-gray-300 text-slate-500 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          placeholder={t("contact.contact_example.telephone")}
          required
        />
      </div>
      <div className="mb-6">
        <label
          htmlFor="email"
          className="block mb-2 text-sm font-medium text-slate-500 dark:text-gray-300"
        >
          {t("contact.your_email")}
        </label>
        <input
          type="email"
          id="email"
          className="bg-gray-50 border border-gray-300 text-slate-500 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          placeholder={t("contact.contact_example.email")}
          required
        />
      </div>
      <div className="mb-6">
        <label
          htmlFor="subject"
          className="block mb-2 text-sm font-medium text-slate-500 dark:text-gray-300"
        >
          {t("contact.your_subject")}
        </label>
        <input
          type="text"
          id="subject"
          className="bg-gray-50 border border-gray-300 text-slate-500 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          placeholder={t("contact.contact_example.subject")}
          required
        />
      </div>
      <div className="mb-6">
        <label
          htmlFor="message"
          className="block mb-2 text-sm font-medium text-slate-500 dark:text-gray-300"
        >
          {t("contact.your_message")}
        </label>
        <input
          type="text"
          id="message"
          className="bg-gray-50 border border-gray-300 text-slate-500 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          placeholder={t("contact.contact_example.message")}
          required
        />
      </div>
      <UtilButton
        text={t("contact.send")}
        action={() => {
          console.log("Enviado");
        }}
      />

    </form>
  );
}
