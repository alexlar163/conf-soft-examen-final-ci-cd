import HomeContactForm from "./HomeContactForm";
import HomeContactInfo from "./HomeContactInfo";
import useTranslation from "next-translate/useTranslation";
import UtilTitle from "./UtilTitle";

export default function HomeContact() {
  const { t } = useTranslation("common");
  return (
    <>
      <div className="m-5 sm:m-0 sm:my-5">
        <UtilTitle text={t("contact.title")} />
        <div className="grid grid-cols-1 lg:grid-cols-2 ">
          <HomeContactInfo />
          <HomeContactForm />
        </div>
      </div>
    </>
  );
}
