import useTranslation from "next-translate/useTranslation";
import trompo from "../public/static/images/trompo.jpg";
import Image from "next/image";
import home from "../public/static/images/home.jpg";
import UtilButton from "./UtilButton";

export default function BrandsInfo({ text, img }) {
  const { t } = useTranslation("common");
  return (
    <>
      <div className="grid grid-cols-4 gap-10 mb-5 mt-20">
        <div className="col-span-3 flex flex-col items-center justify-center">
          <div className="font-extrabold">
            <Image src={img} alt={img} objectFit="cover" />
          </div>
        </div>
        <div className="col-span-1">
          <Image src={trompo} alt={trompo} objectFit="cover" />
        </div>
      </div>
      <div className="grid grid-rows-12 grid-cols-4 grid-flow-row gap-y-4 gap-x-10">
        <div className="col-span-1 row-span-8 relative">
          <div className="relative h-[600px]">
            <Image src={home} alt={home} objectFit="cover" layout="fill" />
          </div>
        </div>
        <div className="col-span-2 row-span-8 relative">
          <div className="relative h-[600px]">
            <Image src={home} alt={home} objectFit="cover" layout="fill" />
          </div>
        </div>
        <div className="col-span-1 row-span-8">
          <p className="text-ellipsis overflow-hidden text-2xl font-light text-slate-500">
            {text}
          </p>
          <UtilButton
            className="mt-4"
            text={t("brands.btnVisitanos")}
            action={() => {
              window.scrollTo({
                top: document.body.scrollHeight,
                behavior: "smooth",
              });
            }}
          />
        </div>
      </div>
    </>
  );
}
