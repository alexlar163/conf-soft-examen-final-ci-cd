export default function UtilButton({ text, action }) {
  return (
    <button
      className="block rounded-md px-6 py-1 uppercase bg-orange-500 hover :bg-orange-600 text-white font-extrabold mx-auto mt-4"
      onClick={action}
    >
      {text}
    </button>
  );
}
