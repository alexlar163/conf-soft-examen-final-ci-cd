import { Fragment, forwardRef } from "react";
import { Menu, Transition } from "@headlessui/react";
import { MenuIcon } from "@heroicons/react/solid";
import useTranslation from "next-translate/useTranslation";
import Link from "next/link";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const NextLink = forwardRef((props, ref) => {
  const { href, children, ...rest } = props;
  return (
    <Link href={href}>
      <a {...rest} ref={ref}>{children}</a>
    </Link>
  );
});
export default function HeaderDropMenu() {
  const { t } = useTranslation("common");

  return (
    <Menu
      as="div"
      className="absolute left-0 inline-block text-left mt-3 ml-3 sm:hidden"
    >
      <Menu.Button className="w-full text-gray-700 hover:bg-gray-400">
        <MenuIcon className="h-8 w-8" aria-hidden="true" />
      </Menu.Button>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="origin-top-right absolute left-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 divide-y divide-gray-100 focus:outline-none z-50">
          {[
            [t("router.home"), "/"],
            [t("router.brands"), "/brands"],
            [t("router.services"), "/services"],
            [t("router.about"), "/about"],
            [t("router.contact"), "/contact"],
          ].map(([title, url]) => (
            <div className="py-1" key={`${title}-${url}`}>
              <Menu.Item>
                {({ active }) => (
                  <NextLink
                    href={url}
                    className={`${
                      active ? "bg-gray-100 text-gray-900" : "text-gray-700"
                    } flex justify-between w-full px-4 py-2 text-sm leading-5 text-left`}
                  >
                    {title}
                  </NextLink>
                )}
              </Menu.Item>
            </div>
          ))}
        </Menu.Items>
      </Transition>
    </Menu>
  );
}
