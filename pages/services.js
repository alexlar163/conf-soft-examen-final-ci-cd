import ServiceContainer from "../components/ServiceContainer";

export default function services() {
  return (
    <>
      <ServiceContainer />
    </>
  );
}
